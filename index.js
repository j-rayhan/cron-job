const CronJob = require('cron').CronJob;
const fetchNewAds = require('./sitemap');

const now = new Date().toISOString();
 
const storeAdsUrl = new CronJob({
    // cronTime: '0 */1 * * * *', // every 1 mins
    cronTime: '0 0 0 * * *', // will run every day at 12:00 AM
    onTick: function () {
        console.log(`
        ----------------------
        current time: ${now} 
        Scheduled Tasks - sending lesson reminder 1 mins
        ---------------------
      `);
      fetchNewAds();
    },
    start: true,
    timeZone: 'Asia/Dhaka',
});

storeAdsUrl.start();
