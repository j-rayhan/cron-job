const fetch = require("node-fetch");
// Slurp in an xml file, update/append to it and pipe it back out
const { createReadStream, createWriteStream, copyFile, unlink } = require('fs');
const { resolve } = require('path');
const { SitemapStream, XMLToSitemapItemStream, streamToPromise } = require('sitemap')
const { tmpdir } = require('os');

const start = new Date(new Date("2020-05-20").setHours(0, 0, 0, 0)).toISOString(),
        end = new Date(new Date("2020-06-10").setHours(23, 59, 59, 999)).toISOString(),
        query = `query {
            ads(where: { 
                createdAt_gt: "${start}", 
                createdAt_lt: "${end}"
            }){
                id
                s3Prefix
                title
                description
            }
        }`;

module.exports = async function() {

  const fetchNewAds = await fetch('http://157.230.246.110:4466/', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    query,
                }),
            })
            .then(response => {
                return response.json()
            })
            .then(responseAsJson => {
                const { data : { ads } } = responseAsJson;
                // console.log('------------->', ads);
                return ads;
            });

  // console.log('PRINT IN %s=====>','ads', fetchNewAds);

  const smStream = new SitemapStream();
  const smPath = resolve('./sitemap.xml');
  const tmpPath = resolve(tmpdir(), './sitemap.xml');
  
  const HOST_NAME = "https://propertyshop.com.bd";
  let links = [];
  
  function generateUrl(s3Prefix, title, description){
    return `?p=${s3Prefix}&t=${title}&d=${description}`;
  }
  
  const pipeline = createReadStream(smPath)
    // this parses the xml and turns it into a stream of objects
    // suitable for SitemapAndIndexStream or SitemapStream
    .pipe(new XMLToSitemapItemStream())
    .pipe(smStream) // turns options back into xml
    .pipe(createWriteStream(tmpPath));

  pipeline.on('finish', () =>
    // overwrite original with temp file
    copyFile(tmpPath, smPath, error => {
      // delete temp file
      unlink(tmpPath, () => {});
    })
  );

  pipeline.on('error', e => e.code === 'EPIPE' || console.error(e));
  
  fetchNewAds.forEach(ad => 
      links.push({ 
        url: HOST_NAME + generateUrl(ad.s3Prefix, ad.title, ad.description),
        img: [
          {
            url: `http://dmlmeyc1hgera.cloudfront.net/public/${ad.s3Prefix}/0`,
            caption: 'An image',
            title: 'The Title of Image One',
          },
        ],
        changefreq: 'daily',
        priority: 0.3 
      }));
  // Here is where the sitemap items get appended to the stream.
  links.forEach( link => smStream.write( link ) );
}